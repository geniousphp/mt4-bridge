'use strict';

var net = require('net');
var util = require('util')
var events = require('events')

var tcpSubscriber = null
var mt4Utils = require('./utils')

function TCPSubscriber(host, port) {
  events.EventEmitter.call(this);
  this.data = ""
  this.isConnected = false
  this.host = host
  this.port = port
  this.subscriber = net.connect(port, host);
  this.subscriber.setKeepAlive(true);
  this.registerEvents()
}



TCPSubscriber.prototype.registerEvents = function() {
  var self = this
  this.subscriber.on('connect', function () {
    self.isConnected = true
    console.log('OK: TCPSubscriber Connected to MT4 Publisher');
  });

  this.subscriber.on('data', function (msg) {
    msg = msg.toString()
    self.data += msg
    if(msg.endsWith('&')){
      var _response
      var _splittedMsg = self.data.split('&')
      /*
      _splittedMsg examples 
      1- [ 'ORDERS|0', '' ]
      2-
        [ '
          ORDERS|
          0|
          265414086,1,0.01,CADCHF,0.69667,0.69682,0.69680,0.69682,0.00000,0.00000,-0.06,0.00,-0.14,0.00000000,1589838827.00000000,|
          265414510,1,0.01,GBPCHF,1.18633,1.18641,1.18633,1.18641,0.00000,0.00000,-0.06,0.00,-0.08,0.00000000,1589839001.00000000,
          ',
          '' 
        ]
      */
      for (var i = 0; i < _splittedMsg.length - 1; i++) {
        _response = mt4Utils.parseMessage(_splittedMsg[i])
        /*
        _response example
        1- [ 'ORDERS', null, true ]
        2- 
        [ 
          'ORDERS',
          null,
          [ 
            '265414086,1,0.01,CADCHF,0.69667,0.69682,0.69677,0.69682,0.00000,0.00000,-0.06,0.00,-0.14,0.00000000,1589838827.00000000,',
            '265414510,1,0.01,GBPCHF,1.18633,1.18617,1.18609,1.18617,0.00000,0.00000,-0.06,0.00,0.15,0.00000000,1589839001.00000000,' 
          ] 
        ]
        */
        if(_response === null) continue
        if(_response[1] !== null){
          return console.error(new Error('TCPSubscriber: Error while getting data ' + _response[1]));
        }
        switch(_response[0]) {
          case 'ACCOUNT':
            self.emit('account', _response[2])
            break;
          case 'ORDERS':
            self.emit('orders', _response[2])
            break;
          case 'SIGNALS_PIPS':
            self.emit('signals_pips', _response[2])
            break;
          case 'PROFIT_CURRENCY':
            self.emit('profit_currency', _response[2])
            break;
          default:
        }
      }
      self.data = ''
    }
  })


  this.subscriber.on('end', function () {console.log('TCPSubscriber Connection End')});
  this.subscriber.on('timeout', function () {console.log('TCPSubscriber Connection timeout')});
  this.subscriber.on('drain', function () {console.log('TCPSubscriber Connection drain')});
  this.subscriber.on('error', function () {console.error('ERROR: TCPSubscriber error connection to MT4 Publisher');});
  this.subscriber.on('close', function () {
    console.log("TCPSubscriber Connection closed");
    self.isConnected = false

    self.subscriber.destroy();
    self.subscriber.unref();
    self.subscriber.removeAllListeners()

    // Re-open socket
    console.log("TCPSubscriber Will try to reconnect after 1s");
    setTimeout(function () {
      self.subscriber = net.connect(self.port, self.host);
      self.subscriber.setKeepAlive(true);
      self.registerEvents()
    }, 1000);
  });
}




util.inherits(TCPSubscriber, events.EventEmitter);

module.exports = function() {
  return {
    connect: function (host, port) {
      if(tcpSubscriber) return tcpSubscriber
      else{
        tcpSubscriber = new TCPSubscriber(host, port);
        return tcpSubscriber
      }
    },
    closeConnection: function () {
      if(tcpSubscriber && tcpSubscriber.isConnected){
        tcpSubscriber.subscriber.write('close&');
      }
    },
  }
}()