var SchemaValidator = require('jsonschema').Validator
var sv = new SchemaValidator()

const isArraySorted = require('is-array-sorted');

var operationEnum = require('./enums/operation')
var responseEnum = require('./enums/response')

var tradedSymbols = require('./tradedSymbols')

var signalSchema = {
  'id': '/signal',
  'type': 'object',
  'properties': {
    'provider': {'type': 'number'},
    'symbol': {'type': 'string'},
    'entryPrice': {'type': 'number'},
    'operation': {
      'type': 'number',
      'minimum': operationEnum.OP_BUY,
      'maximum': operationEnum.OP_SELL
    },
    'sl': {'type': 'number'},
    'tps': {
      "type": "array",
      "items": {"type": "number"},
      "minItems": 1,
      "maxItems": 3
    }
  },
  'required': ['symbol', 'entryPrice', 'operation', 'sl', 'tps']
}


function reverseArray(arr) {
  var newArray = [];
  for (var i = arr.length - 1; i >= 0; i--) {
    newArray.push(arr[i]);
  }
  return newArray;
}


module.exports = {

  pipsToUnits: function (symbol, pips) {
    if(symbol.includes('JPY')) return (pips/100)
    else return (pips/10000)
  },
  unitsToPips: function (symbol, units) {
    if(symbol.includes('JPY')) return (units*100)
    else return (units*10000)
  },

  roundPips: function (pips) {
    return Math.round(pips * 10) / 10
  },

  calculateProfitPips: function (symbol, operation, positionPrice, closedPrice) {
    //This method can be used also to compare prices
    var pipsDiff = this.roundPips(this.unitsToPips(symbol, closedPrice - positionPrice))
    switch(operation) {
      case operationEnum.OP_BUY:
        return pipsDiff
        break;
      case operationEnum.OP_SELL:
        return -1 * pipsDiff
        break;
      default:
        console.error('Unhandled Operation', operation, '-', 'Must be BUY or SELL');
    }
    return 
  },
  
  roundPrice: function (symbol, price) {
    if(symbol.includes('JPY')) return Math.round(price * 1000)/1000
    else return Math.round(price * 100000)/100000
  },

  roundLot: function (lot) {
    // 0.0876754 => 0.09
    //MT4 accepts only 2 digits precision
    return Math.round(lot * 100)/100
  },
  roundUpLot: function (lot) {
    return Math.ceil(lot * 100)/100
  },
  roundDownLot: function (lot) {
    return Math.floor(lot * 100)/100
  },

  isPipsDifferenceTolerated: function (symbol, price1, price2, pipsTolerence) {
    return ( (Math.abs(price1 - price2) <= this.pipsToUnits(symbol, pipsTolerence)) 
      ? true 
      : false  )
  },

  calculateNewPrice: function (symbol, operation, entryPrice, relativeTarget, factor) {
    /*
      Example1: 1.1520 -> 1.1590 
      0.45 (45%)
      => 1.1520 + (1.1590 - 1.1520) * 0.45 = 1.15515 = 1.1552

      Example2: 1.1520 -> 1.1430 
      0.45 (45%)
      => 1.1520 - (1.1520 - 1.1430) * 0.45 = 1.14795 = 1.1480
    */

    switch(operation) {
      case operationEnum.OP_BUY:
        return this.roundPrice(symbol, entryPrice + (relativeTarget - entryPrice) * factor )
        break;
      case operationEnum.OP_SELL:
        return this.roundPrice(symbol, entryPrice - (entryPrice - relativeTarget) * factor )
        break;
      default:
        console.error('Unhandled Operation', operation, '-', 'Must be BUY or SELL');
    }
  },

  calculateNewPriceByPips: function (symbol, operation, price, pips) {
    /*
      Example1: 1.1520 & 20 pips -> 1.1540  if buy
    */

    switch(operation) {
      case operationEnum.OP_BUY:
        return this.roundPrice(symbol, price + this.pipsToUnits(symbol, pips) )
        break;
      case operationEnum.OP_SELL:
        return this.roundPrice(symbol, price - this.pipsToUnits(symbol, pips) )
        break;
      default:
        console.error('Unhandled Operation', operation, '-', 'Must be BUY or SELL');
    }
  },

  normalizeOperation: function (operation) {
    return operation % 2;
  },

  isTpHit: function (operation, currentPrice, tp) {
    switch(operation) {
      case operationEnum.OP_BUY:
        return ( currentPrice >= tp ) ? true : false
        break;
      case operationEnum.OP_SELL:
        return ( currentPrice <= tp ) ? true : false
        break;
      default:
        console.error('Unhandled Operation', operation, '-', 'Must be BUY or SELL');
    }
  },

  isSlHit: function (operation, currentPrice, sl) {
    switch(operation) {
      case operationEnum.OP_BUY:
        return ( currentPrice < sl ) ? true : false
        break;
      case operationEnum.OP_SELL:
        return ( currentPrice > sl ) ? true : false
        break;
      default:
        console.error('Unhandled Operation', operation, '-', 'Must be BUY or SELL');
    }
  },

  isSlTriggerHit: function (operation, currentPrice, onSlTrigger) {
    var tradeId = null
    onSlTrigger = onSlTrigger.sort(function(trade1, trade2){return trade1.On - trade2.On});
    if(operation === operationEnum.OP_SELL) onSlTrigger.reverse()

    for (var i = 0; i < onSlTrigger.length; i++) {
      if((onSlTrigger[i].state === 'PENDING') && this.isSlHit(operation, currentPrice, onSlTrigger[i].On)){
        tradeId = onSlTrigger[i].trade
        break
      }
    }
    return tradeId
  },

  parseMessage: function (message) {
    //workaround, otherwise get response like "[ '0', '0', '108.232000', '108.232000', 'USDJPY\u0000' ]"
    const split = message.toString().replace(/\u0000/, '').split('|')
    if(split.length < 3){
      // return null //msg must be [id, err, errCode/res1, res2,...]
      split.push('') //in order to be able to receive empty response (no orders)
    }
    const id = split.shift()
    const status = +split[0]
    let err, msg
    if (status === responseEnum.RESPONSE_OK) {
      err = null
      msg = split.length === 2 && split[1] === "" ? true : split
      if (msg !== true) {
        msg.shift()
      }
    } else if (status === responseEnum.RESPONSE_FAILED) {
      err = split[1]
      msg = null
    }
    return [ id, err, msg ]
  },

  isMT4CommentValid: function (comment) {
    if(comment.length <= 27) return true
    return false
  },

  validateSymbol: function (symbol) {
    symbol = symbol.replace(/\//g, '').toUpperCase()
    if(symbol.length !== 6) return null
    if (tradedSymbols.indexOf(symbol) < 0) return null
    return symbol
  },

  validateSignal: function (signal) {
    if (sv.validate(signal, signalSchema).errors.length > 0){
      console.error(JSON.stringify(signal));
      console.error('Signal Validation', sv.validate(signal, signalSchema).errors);
      return null
    }
    signal.symbol = this.validateSymbol(signal.symbol)
    if(signal.symbol === null){
      console.error(JSON.stringify(signal));
      console.error('Signal Validation: Invalid Symbol');
      return null
    }

    if(signal.operation === operationEnum.OP_BUY) {
      if(signal.sl >= signal.entryPrice){
        console.error(JSON.stringify(signal));
        console.error('Signal Validation: Invalid SL');
        return null
      }
      if(signal.tps[0] <= signal.entryPrice || !isArraySorted(signal.tps)){
        console.error(JSON.stringify(signal));
        console.error('Signal Validation: Invalid TPs');
        return null
      }
    }
    else if (signal.operation === operationEnum.OP_SELL) {
      if(signal.sl <= signal.entryPrice){
        console.error(JSON.stringify(signal));
        console.error('Signal Validation: Invalid SL');
        return null
      }
      var reversedTPs = reverseArray(signal.tps)
      if(signal.tps[0] >= signal.entryPrice || !isArraySorted(reversedTPs)){
        console.error(JSON.stringify(signal));
        console.error('Signal Validation: Invalid TPs');
        return null
      }
    }
    return signal
  },

  calculateSafeLot: function (fixedLot, lot, riskPercentage, capital, entryPrice, sl, symbol) {
    if(fixedLot){
      return lot
    }
    else{
      return ((riskPercentage * capital) / 100 ) / 
        ( Math.abs(this.unitsToPips(symbol, sl - entryPrice)) * 10)
    }
  },

  negateOperation: function (operation) {
    switch(operation) {
      case operationEnum.OP_BUY:
        return operationEnum.OP_SELL
        break;
      case operationEnum.OP_SELL:
        return operationEnum.OP_BUY
        break;
      default:
        console.error('Unhandled Operation', operation, '-', 'Must be BUY or SELL');
    }
  }

}


