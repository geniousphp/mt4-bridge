module.exports = {
  'REQUEST_PING': 1, // Ping MT4 client.
  'REQUEST_HEALTH': 2, // Check MT4 client Health.
  'REQUEST_TRADE_OPEN': 11, //  Create new order.
  'REQUEST_TRADE_MODIFY': 12, //  Modify placed order.
  'REQUEST_TRADE_DELETE': 13, //  Delete pending order.
  'REQUEST_DELETE_ALL_PENDING_ORDERS': 21, //  Delete all pending orders.
  'REQUEST_CLOSE_MARKET_ORDER': 22, //  Close open market orders.
  'REQUEST_CLOSE_ALL_MARKET_ORDERS': 23, //  Close all open market orders.
  'REQUEST_RATES': 31, //  Get current rate for the symbol.
  'REQUEST_ACCOUNT': 41, //  Get account info.
  'REQUEST_ORDERS': 51, //  Get account orders.
  'REQUEST_HISTORY_ORDER': 62 //  Get order from history.
}
