var net = require('net');

var operationEnum = require('./enums/operation')
var requestEnum = require('./enums/request')
var responseEnum = require('./enums/response')
var unitsEnum = require('./enums/units')
var errorCodes = require('./errorCodes')

var utils = require('./utils')

var socket
var isConnected = false
var requestId = 0
var callbacksHash = {}
var TcpOptions = {
  host: null,
  port: null,
  options: {}
}

var glbResponses = ''

function onConnect(socket) {
  isConnected = true
  console.log('OK: Socket Connected to MT4 Server');
}

function onClose(socket) {
  console.log("TCP Connection closed");
  isConnected = false

  socket.destroy();
  socket.unref();
  socket.removeAllListeners()

  // Re-open socket
  console.log("Will try to reconnect after 1s");
  setTimeout(openSocket.bind(null, TcpOptions.host, TcpOptions.port, TcpOptions.options), 1000);
}

function onResponse(responses) {
  if(responses.toString().endsWith('&')){
    glbResponses += responses.toString()
    var response
    for (var i = 0; i < glbResponses.split('&').length - 1; i++) {
      response = utils.parseMessage(glbResponses.split('&')[i])
      var callback = callbacksHash[response[0]]
      if(callback && typeof callback === 'function'){
        if(response[1])
          callback(new Error(errorCodes[response[1]][0]), response[0], response[2])
        else
          callback(response[1], response[0], response[2])
      }
      callbacksHash[response[0]] = null
    }
    glbResponses = ''
  }
  else{
    glbResponses += responses.toString()
  }
}

function incRequestId(message)  {
  if(requestId >= Infinity) return 0
  else requestId++
}

function openSocket(host, port, options) {
  socket = net.connect(port, host);
  socket.setKeepAlive(true);
  socket.on('connect', onConnect.bind({}, socket));
  socket.on('data', onResponse);
  socket.on('end', function () {console.log('TCP Connection End')});
  socket.on('timeout', function () {console.log('TCP Connection timeout')});
  socket.on('drain', function () {console.log('TCP Connection drain')});
  socket.on('error', function () {console.error('ERROR: Socket error connection to MT4 Server');});
  socket.on('close', onClose.bind({}, socket));
}




module.exports = function () {

  return {
    openSocket: function (host, port, options) {
      TcpOptions.host = host
      TcpOptions.port = port
      TcpOptions.options = Object.assign({
        requestsTimeout: 25
      }, options);
      openSocket(host, port, TcpOptions.options)
    },

    request: function (request, callback) {
      if(!isConnected) 
        return callback(new Error('Socket not Connected to MT4 TCP Server'), -1)
      var currentRequestId = requestId
      request.splice(0, 0, currentRequestId);
      socket.write(request.join('|') + '&');
      if(callback && typeof callback === 'function'){
        callbacksHash[currentRequestId] = callback
      }
      incRequestId()
      setTimeout(function() {
        if(callbacksHash[currentRequestId] && typeof callbacksHash[currentRequestId] === 'function'){
          callbacksHash[currentRequestId](new Error('TCP Request Timeout'), currentRequestId)
        }
      }, TcpOptions.options.requestsTimeout * 1000);
    },

    closeConnection: function () {
      if(isConnected){
        socket.write('close&');
      }
    },

    requestRate: function (symbol, callback) {
      var request = []
      request.push(requestEnum['REQUEST_RATES'])
      request.push(symbol)
      this.request(request, callback)
    },

    openTrade: function (trade, callback) {
      var request = []
      
      request.push(requestEnum['REQUEST_TRADE_OPEN'])
      request.push(trade.symbol)
      request.push(trade.operation)
      request.push(trade.lot)
      request.push(trade.entryPrice) //This param doesn't matter, It will buy/sell at market price
      request.push(trade.params.slippage)
      request.push(0) //Don't set SL
      request.push(0) //Don't set TP
      request.push(trade.comment) //comment
      request.push(trade.magic) //magic number
      request.push(unitsEnum['UNIT_CONTRACTS'])
      this.request(request, callback)
    },

    closeTrade: function (orderId, price, lot, callback) {
      var request = []
      request.push(requestEnum['REQUEST_CLOSE_MARKET_ORDER'])
      request.push(orderId)
      request.push(price)
      request.push(lot)
      this.request(request, callback)
    },

    requestOrderHistory: function (orderId, callback) {
      var request = []
      request.push(requestEnum['REQUEST_HISTORY_ORDER'])
      request.push(orderId)
      this.request(request, callback)
    },

    ping: function (callback) {
      var request = []
      request.push(requestEnum['REQUEST_PING'])
      this.request(request, callback)
    },

    health: function (callback) {
      var request = []
      request.push(requestEnum['REQUEST_HEALTH'])
      this.request(request, callback)
    },

    isConnected: function () {
      return isConnected
    }

  }

}()
