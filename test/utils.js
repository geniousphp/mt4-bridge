var mt4Bridge = require('../index.js')

var utils = mt4Bridge.utils

var operationEnum = require('../libs/enums/operation')

console.log(utils.unitsToPips('EURUSD', 0.0004));

console.log(utils.validateSignal({
  provider: 123456,
  symbol: 'EURUSD',
  entryPrice: 1.1200,
  operation: operationEnum.OP_BUY,
  sl: 1.1140,
  tps: [1.1220, 1.1250, 1.1300]
}));


console.log(utils.validateSignal({
  provider: 123456,
  symbol: 'AUDNZD',
  entryPrice: 1.0565,
  operation: 1,
  sl: 1.061,
  tps: [1.0545,1.0515,1.0465]
}));


console.log(utils.calculateSafeLot(false, null, 2, 1000, 1.1200, 1.1150, 'EURUSD'));

console.log(utils.calculateNewPriceByPips(
  'EURUSD',
  operationEnum.OP_SELL,
  1.1250,
  -10
));


console.log(utils.calculateProfitPips(
  'GBPCAD', 
  operationEnum.OP_SELL, 
  1.68618, 
  1.68518
)); //10