var async = require('async')
var mt4Bridge = require('../index.js')

var SubscriberModule = mt4Bridge.subscriber

var subscriber = SubscriberModule.connect('192.168.1.92', 45556)

subscriber.on('orders', function (orders) {
  console.log("orders", orders);
})

// subscriber.on('signals_pips', function (signalsPips) {
//   console.log(signalsPips);
// })

subscriber.on('profit_currency', function (profit_currency) {
  console.log(profit_currency);
})

var signals = ['SIGINT', 'SIGTERM']
signals.forEach(function (signal) {
  process.on(signal, function () {
    SubscriberModule.closeConnection()
    process.exit(0)
  })
});
